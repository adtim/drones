FROM openjdk:17

WORKDIR /app

COPY . .

RUN ./mvnw install -DskipTests
CMD ["sh", "-c", "./mvnw spring-boot:run"]

